package net.obs_cop.m.nakano.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloController {
	 @Autowired
	 UserInfoMapper userInfoMapper;

	@GetMapping("/")
	public String index(Model model) {
        List<UserInfo> list = userInfoMapper.selectAll();
        model.addAttribute("userInfo", list);
        return "index";
	}

}
