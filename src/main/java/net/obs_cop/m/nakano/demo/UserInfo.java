/**
 * 
 */
package net.obs_cop.m.nakano.demo;

/**
 * @author bkbin
 * @see https://itsakura.com/eclipse_getterandsetter
 */
public class UserInfo {
	private String mail_address;
	private String passwd;
	private String user_name;
	private int manage_level;
	private int user_number;
	public String getMail_address() {
		return mail_address;
	}
	public void setMail_address(String mail_address) {
		this.mail_address = mail_address;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public int getManage_level() {
		return manage_level;
	}
	public void setManage_level(int manage_level) {
		this.manage_level = manage_level;
	}
	public int getUser_number() {
		return user_number;
	}
	public void setUser_number(int user_number) {
		this.user_number = user_number;
	}
}
