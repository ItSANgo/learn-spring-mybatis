package net.obs_cop.m.nakano.demo;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface  UserInfoMapper {
	// 2022-03-26 動作するようになった。
	// XML周りの依存関係がおかしいという情報がある。
	// 正直なところ気持ち悪い。
	// see https://teratail.com/questions/169846
	// https://stackoverflow.com/questions/69668380/servlet-service-error-classnotfoundexception
	@Select("SELECT mail_address, passwd, user_name,manage_level, user_number FROM obs.users")
    public List<UserInfo> selectAll();
}
