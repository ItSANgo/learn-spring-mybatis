
CREATE TABLE users (	
    mail_address VARCHAR(191) PRIMARY KEY,	-- name@obs-cop.net
	passwd VARCHAR(512) NOT NULL, -- password
	user_name VARCHAR(1024),  -- real name
	manage_level INTEGER NOT NULL, -- 0: normal user, 1 modelator
	user_number INTEGER NOT NULL -- unique number
);
	
CREATE TABLE threads (	
    thread_id INTEGER AUTO_INCREMENT PRIMARY KEY,	
	title VARCHAR(1024) NOT NULL
);	
	
CREATE TABLE articles (	
    article_id INTEGER AUTO_INCREMENT PRIMARY KEY,	
	thread_id INTEGER NOT NULL,
	author INTEGER NOT NULL,
	post_time TIMESTAMP NOT NULL,
	message_body VARCHAR(4000) NOT NULL
);
