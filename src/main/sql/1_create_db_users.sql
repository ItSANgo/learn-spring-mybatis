CREATE DATABASE obs CHARACTER SET utf8mb4;

GRANT ALL ON obs.* TO obs@localhost IDENTIFIED BY 'obs' WITH GRANT OPTION;
GRANT ALL ON obs.* TO obsselect@localhost IDENTIFIED BY 'obs';
